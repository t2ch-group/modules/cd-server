const express = require('express');
const bodyParser = require('body-parser');
const spawn = require('await-spawn');
const app = express();
const config = require('./config.js').config;

app.use(
    bodyParser.json({limit: '50mb', extended: true}),
    bodyParser.urlencoded({ extended: true }),
    async (req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        next();
    }
)

config.webhooks.map( hook => {
    app.post(hook.path, async (req, res, next) => {
        if (req.headers['x-gitlab-token'] === hook.token) {
            res.status(200);
            res.send('ok');
            let gitFetch = await spawn('git', ['fetch', `https://${hook.user}:${hook.pass}@${hook.host}`], { cwd: hook.cwd});
            spawnLitsten(gitFetch, 'fetch');
            let gitStash = await spawn('git', ['stash'], { cwd: hook.cwd});
            spawnLitsten(gitStash, 'stash');
            let gitPull = await spawn('git', ['pull', `https://${hook.user}:${hook.pass}@${hook.host}`], { cwd: hook.cwd});
            spawnLitsten(gitPull, 'pull');
            let gitStatus = await spawn('git', ['status'], { cwd: hook.cwd});
            spawnLitsten(gitStatus, 'status');
            try {
                let updateServerModules = await spawn('npm', ['i'], { cwd: hook.cwd+'/server'})
                spawnLitsten(updateServerModules, 'npm i server');
            } catch (e) {}
            try {
                let updateModules = await spawn('npm', ['i'], { cwd: hook.cwd+'/client'})
                spawnLitsten(updateModules, 'npm i front');
            } catch (e) {}
            try {            
                let buildFront = await spawn('npm', ['run', 'build'], { cwd: hook.cwd+'/client'})
                spawnLitsten(buildFront, 'npm build');
            } catch (e) {}
            console.log(`Обновил ветку после пуша ${req.body.user_username}: ${new Date().toISOString()}`);
        } else res.status(403);
    })
})

function spawnLitsten(listener, text){
    console.log(text, listener.toString())
    /*listener.stdout.on('data', (data) => {
        console.log(`${text} stdout: ${data}`);
    });
    listener.stderr.on('data', (data) => {
        console.log(`${text} stderr: ${data}`);
    });
    listener.on('close', (code) => {
        console.log(`${text} child process exited with code ${code}`);
    });*/
}

app.use('*', (req, res, next) => {
    console.log(req.body)
    console.log('---')
    console.log(req.headers)
    console.log('---')
    console.log(req.params)
    console.log('---')
    console.log(req.query)
    console.log('------')
    res.status(200);
    res.send('ok')
})

app.listen(config.port);
console.log(`Gitlab CD on :${config.port}`)