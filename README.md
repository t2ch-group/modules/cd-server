exports.config = {
    webhooks: [
        {
            path: '/space-chat',
            token: 'space-chat-gitlab-webhook',
            cwd: '/root/space-chat',
            user: '',
            pass: '',
            host: 'gitlab.com/t2ch-group/projects/space-chat'
        }
    ],
    port: 9000
}